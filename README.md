# OOVulkan GTK Extension
The project is a [GTK](https://www.gtk.org/) extension plugin for the [OOVulkan](https://gitlab.com/oovulkan/oovulkan) project.  
See the [OOVulkan Main Page](https://gitlab.com/oovulkan/oovulkan) for more details.