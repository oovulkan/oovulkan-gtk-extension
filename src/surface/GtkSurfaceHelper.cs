﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using GLib;
using oovulkan.device;
using oovulkan.queue.family;
using oovulkan.surface.xlib;
using oovulkan.util;

namespace oovulkan.surface
{
    public static class GtkSurfaceHelper
    {
        private static readonly PropertyInfo NativeTypeProp = typeof(Gdk.Window).GetAnyProperty("NativeType");
        private static readonly GType X11Window = GType.FromName("GdkX11Window");
        private enum GtkType{ X11 }

        [DllImport("libgtk-3.so")]
        private static extern IntPtr gdk_x11_display_get_xdisplay(IntPtr window);
        [DllImport("libgtk-3.so")]
        private static extern uint gdk_x11_window_get_xid(IntPtr window);

        private static XlibDisplayHandle GetX11Handle(this Gdk.Display display) => gdk_x11_display_get_xdisplay(display.Handle);
        private static XlibVisualHandle GetX11Handle(this Gtk.Window window) => gdk_x11_window_get_xid(window.Window.Handle);

        private static GtkType GetGtkType(Gtk.Window window)
        {
            if(window.Window == null)
                window.ShowAll();
            GType type = (GType)NativeTypeProp.Get(window.Window);
            if(type == X11Window)
                return GtkType.X11;
            else
                throw new NotSupportedException(type.ToString());
        }

        public static bool SupportsGtk(this QueueFamily family, Gtk.Window window)
        {
            var type = GetGtkType(window);
            switch(type)
            {
                case GtkType.X11:
                    return family.SupportsXlib(window.Display.GetX11Handle(), window.GetX11Handle());
                default:
                    throw new NotSupportedException(type.ToString());
            }
        }
        public static Surface GetGtkSurface(this Device device, Gtk.Window window)
        {
            var type = GetGtkType(window);
            switch(type)
            {
                case GtkType.X11:
                    return device.GetXlibSurface(window.Display.GetX11Handle(), window.GetX11Handle());
                default:
                    throw new NotSupportedException(type.ToString());
            }
        }
    }
}